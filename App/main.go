package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

var your_name string

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintf("<h1>Hello janbo from %s!!!", your_name)))
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "11130"
	}

	flag.StringVar(&your_name, "your_name", "@YourName", "Your name for string of homepage")
	flag.Parse()

	mux := http.NewServeMux()

	mux.HandleFunc("/", indexHandler)
	http.ListenAndServe(":"+port, mux)
}
