terraform {
  required_providers {
    aws = {
      version = "~> 2.0"
      source = "hashicorp/aws"
    }
  }

  backend "http" {}
}

provider "aws" {
  region     = var.region
}

provider "tls" {} 

