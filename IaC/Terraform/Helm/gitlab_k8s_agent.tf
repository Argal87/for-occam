// It creates a GitLab Agent for Kubernetes in this project,
// creates a token and install the `gitlab-agent` Helm Chart.
// (see https://gitlab.com/gitlab-org/charts/gitlab-agent)
data "gitlab_project" "this" {
  path_with_namespace = "${var.project_namespace}/${var.project_name}"
}

resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = "${var.cluster_name}"
}

resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = "${var.cluster_name}-token"
  description = "Token for the ${var.cluster_name} used with `gitlab-agent` Helm Chart"
}

resource "helm_release" "gitlab_agent" {
  name             = "gitlab-agent"
  namespace        = "gitlab-agent"
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  version          = "1.9.2"

  set {
    name  = "config.token"
    value = gitlab_cluster_agent_token.this.token
  }
}

# deploy ingress controller
resource "helm_release" "ingress" {
  name       = "ingress-nginx"
  namespace  = "ingress-nginx"
  create_namespace = true
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.0.17"

  values = [
    "${file("./ingress.yaml")}"
  ]
}

#deploy cert maanger
resource "helm_release" "cert" {
  name       = "cert-manager"
  namespace  = "cert-manager"
  create_namespace = true
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.8.0"
  depends_on = [helm_release.ingress]

  set {
    name  = "installCRDs"
    value = "true"
  }

  provisioner "local-exec" {
    command = "echo 'Waiting for cert-manager validating webhook to get its CA injected, so we can start to apply custom resources ...' && sleep 60"
  }
}

#deploy cert issuer
resource "kubectl_manifest" "cluster_issuer_letsencrypt_prod" {
  depends_on = [ helm_release.cert ]
  yaml_body  = <<YAML
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: ${var.email}
    privateKeySecretRef:
      name: letsencrypt-prod
    solvers:
    - http01:
        ingress:
          class:  nginx
YAML
}