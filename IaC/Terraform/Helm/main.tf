terraform {
  backend "http" {}
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "15.7.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "gitlab" {
  early_auth_check = false
}

provider "helm" {
  kubernetes {
    config_path = "../kubeconfig"
  }
}

provider "kubectl" {
  config_path = "../kubeconfig"
}