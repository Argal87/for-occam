output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_eip.main_eip.public_ip
}

output "key_private_pem" {
  value = tls_private_key.key01.private_key_pem
  sensitive = true
}