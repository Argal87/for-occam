# EC2 instance
resource "aws_instance" "main" {
  ami                    = "ami-0162ae2b9d245b9a5"
  instance_type          = "t4g.medium"
  subnet_id              = aws_subnet.dmz.id
  key_name               = aws_key_pair.main.key_name
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = var.main_instance_private_ip
  iam_instance_profile = aws_iam_instance_profile.minikube_profile.name
  root_block_device {
    volume_type = "gp2"
    volume_size = "30"
    delete_on_termination = true
  }

  lifecycle {
    ignore_changes = [
      ami,
      user_data,
      associate_public_ip_address,
    ]
  }

  tags = {
    "Name"                                               = var.cluster_name
    format("kubernetes.io/cluster/%v", var.cluster_name) = "shared"
  }
}

resource "aws_iam_role" "minikube_role" {
  name = var.cluster_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_policy" "minikube_policy" {
  name        = var.cluster_name
  path        = "/"
  description = "Policy for role ${var.cluster_name}"
  policy      = file("./Spec/policy.json.tpl")
}

resource "aws_iam_policy_attachment" "minikube-attach" {
  name = "minikube-attachment"
  roles = [aws_iam_role.minikube_role.name]
  policy_arn = aws_iam_policy.minikube_policy.arn
}

resource "aws_iam_instance_profile" "minikube_profile" {
  name = var.cluster_name
  role = aws_iam_role.minikube_role.name
}

resource "aws_key_pair" "main" {
  key_name   = "deployer-key"
  public_key = "${tls_private_key.key01.public_key_openssh}"
}

resource "tls_private_key" "key01" {
  algorithm   = "RSA"
  rsa_bits = "2048"
}

resource "null_resource" "main" {

  triggers = {
    public_ip = aws_instance.main.public_ip
  }

  lifecycle {
    ignore_changes = [triggers]
  }

  connection {
    type  = "ssh"
    host  = aws_eip.main_eip.public_ip
    user  = var.ssh_user
    port  = "22"
    private_key = tls_private_key.key01.private_key_pem
  }

  provisioner "file" {
    content      = "${templatefile("./Spec/init-aws-minikube.sh", { kubeadm_token = local.k8s_token, dns_name = var.dns_name, ip_address = aws_eip.main_eip.public_ip, cluster_name = var.cluster_name, kubernetes_version = var.kubernetes_version, noip_username = var.noip_username, noip_password = var.noip_password } )}"
    destination = "/tmp/init-aws-minikube.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sed -i -e 's/\\r$//' /tmp/init-aws-minikube.sh",
      "chmod +x /tmp/init-aws-minikube.sh",
      "sudo /tmp/init-aws-minikube.sh",
    ]
  }
}

resource "null_resource" "download_kubeconf" {

  provisioner "local-exec" {
    command = "eval `ssh-agent` && echo \"${tls_private_key.key01.private_key_openssh}\" | tr -d '\\r' | ssh-add - && scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${var.ssh_user}@${aws_eip.main_eip.public_ip}:/home/centos/kubeconfig ./kubeconfig && ls"
  }
  depends_on = [null_resource.main]
}

resource "aws_security_group" "base" {
  name        = "Base SG"

  # Allow inbound
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 32000
    to_port     = 32000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.main.id
}

# Elastic IP
resource "aws_eip" "main_eip" {
  vpc = true
}

resource "aws_eip_association" "main_eip_assoc" {
  instance_id   = aws_instance.main.id
  allocation_id = aws_eip.main_eip.id
}
