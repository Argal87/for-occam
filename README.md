## Instruction for use
1. - set up an account on gitlab.com
   - Make a fork of this project
   - create custom token for curren user, save it as 'TOKEN' in project environment variable

2. - set up an account in the aws cloud
   - configured account for pipeline execution.
     The new accounting entry must have administrator rights. Generate access_key_id and secret_access_key
     Store the values of the data represented as AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in the gitlab project environment.
   
3. - set up an account on no-ip.com
   - configure dynamic entry to any ip (for example 1.1.1.1)
   - Save the account login and password as TF_VAR_noip_password and TF_VAR_noip_username gitlab project environment variables

4. In the .gitlab-ci.yml file, on line #4, replace the TF_VAR_dns_name value with the domain name used in part 3 when creating the entry.
5. In the .gitlab-ci.yml file, on line #32, replace the TF_VAR_email value with your actual email address to send from letsEncrypt.
   
## Not necessary
6. Add YOUR_NAME as the gitlab project environment variable to disply it on you web app page.
7. Replace other variables in .gitlab-ci.yml, wich marked as '#!!! [optional]'
8. Replace instance_type of aws_instance.main terraform object in ec2.tf file

## Important information
- The Kubernetes cluster will be installed on an ec2 instance with an ARM64 chip.
Therefore, all applications that are supposed to run in a cluster must also be built for the ARM64 architecture.
- The first time you use aws ami image, you must accept an agreement.
Therefore, if you have not previously used centos7 arm64, then the first pipeline will fail. In the output of the error there will be a link that you need to follow and agree to the agreement.
